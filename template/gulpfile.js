var gulp = require('gulp');
var cleancss = require('gulp-clean-css');
var rimraf = require('gulp-rimraf');
var ignore = require('gulp-ignore');
var htmlmin = require('gulp-htmlmin');
var gcmq = require('gulp-group-css-media-queries');
var gulpif = require('gulp-if');
var sass = require('gulp-sass');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var bulkSass = require('gulp-sass-bulk-import');
var open = require('gulp-open');
var connect = require('gulp-connect');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var pug = require('gulp-pug');
var useref = require('gulp-useref');
var browser = require('browser-sync').create();
var wiredep = require('wiredep').stream;
var sequence = require('run-sequence');


// Port to use for the development server.
var PORT = {
    dev: 8000,
    prod: 7000,
    ui: 3000
};

// Port to use for the development server.
var DIST = {
    dir: 'dist/',
    assetsDir: 'dist/assets/',
    cssDir: 'dist/assets/css/',
    imgDir: 'dist/assets/img/',
    jsDir: 'dist/assets/js/',
    cssFile: 'custom.css',
    jsFile: 'custom.js'
};

// Browsers to target when prefixing CSS.
var COMPATIBILITY = ['last 2 versions', 'ie >= 9'];

// File paths to various assets are defined here.
var PATHS = {
    style: [
        'src/styles/index.sass'
    ],
    javascript: [
        'src/blocks/**/*.js'
    ]
};

// Copy images to the "dist" folder
gulp.task('images', function () {
    return gulp.src('src/blocks/**/src/*')
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(DIST.imgDir));
});

// Delete the "dist" folder
// This happens every time a build starts
gulp.task('clean', function () {
    return gulp.src(DIST.dir, {read: false})
        .pipe(rimraf());
});

gulp.task('clean:img', function () {
    return gulp.src('dist/assets/img/*', {read: false})
        .pipe(rimraf());
});

gulp.task('clean:pages', function () {
    return gulp.src(DIST.dir + '**.html', {read: false})
        .pipe(ignore(DIST.assetsDir))
        .pipe(rimraf());
});

gulp.task('clean:dev', function () {
    var devs = [
        DIST.jsDir + DIST.jsFile,
        DIST.cssDir + DIST.cssFile
    ];
    return gulp.src(devs, {read: false})
        .pipe(rimraf());
});

// Compile Sass into CSS
gulp.task('sass', function () {
    return gulp.src(PATHS.style)
        .pipe(bulkSass())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: COMPATIBILITY
        }))
        .pipe(rename(DIST.cssFile))
        .pipe(gulp.dest(DIST.cssDir));
});

// Combine JavaScript into one file
gulp.task('javascript', function () {
    return gulp.src(PATHS.javascript)
        .pipe(sourcemaps.init())
        .pipe(concat(DIST.jsFile))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(DIST.jsDir));
});

// Make pug pages
gulp.task('pages', function () {
    return gulp.src('src/pages/*.pug')
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest(DIST.dir))
        .pipe(wiredep())
        .pipe(gulp.dest(DIST.dir));
});

gulp.task('buildProd', function (done) {
    sequence('buildProdAssets', ['buildProdHtml'], 'clean:dev', done);
});

gulp.task('buildProdAssets', function () {
    var optionsCss = {
        keepSpecialComments: 0 //Remove special comments
    };

    var mini = imagemin({
        progressive: true
    });

    return gulp.src(DIST.dir + '**/*.html')
        .pipe(useref())
        .pipe(gulpif('assets/img/**/*', mini))
        .pipe(gulpif('**/*.js', uglify().on('error', function (e) {
            console.log(e);
        })))
        .pipe(gulpif('**/*.css', gcmq()))
        .pipe(gulpif('**/*.css', cleancss()))
        .pipe(gulp.dest(DIST.dir));
});

gulp.task('buildProdHtml', function () {
    var optionsHtml = {
        collapseWhitespace: true,
        minifyCSS: true,
        minifyJS: true,
        removeComments: true,
        removeAttributeQuotes: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true
    };

    return gulp.src(DIST.dir + '*.html')
        .pipe(htmlmin(optionsHtml))
        .pipe(gulp.dest(DIST.dir));
});

// Build the "dist" folder by running all of the above tasks
gulp.task('build', function (done) {
    sequence('clean', ['images', 'sass', 'javascript'], 'pages', done);
});

// Start a server with LiveReload to preview the site in
gulp.task('server', ['build'], function () {
    browser.init({
        startPath: DIST.dir,
        server: {
            baseDir: './'
        },
        reloadDelay: 1000,
        port: PORT.dev,
        index: 'pageMain.html',
        open: 'ui',
        ui: {
            port: PORT.ui
        },
        reloadOnRestart: true
    });
});

//Open dist folder
gulp.task('open', function (done) {
    sequence('connect', ['openHost'], done);
});

gulp.task('connect', function () {
    connect.server({
        name: 'prod',
        root: './',
        port: PORT.prod
    });
});

gulp.task('openHost', function () {
    gulp.src('./')
        .pipe(open({
            uri: 'http://localhost:' + PORT.prod
        }));
});

// Build the site, run the server, and watch for file changes
gulp.task('default', ['build', 'server'], function () {
    gulp.watch(['src/{layout,pages}/**/*', 'src/blocks/**/*.pug'], ['clean:pages', 'pages', browser.reload]);
    gulp.watch(['src/{blocks,styles}/**/*.sass'], ['sass', browser.reload]);
    gulp.watch(['src/blocks/**/*.js'], ['javascript', browser.reload]);
    gulp.watch(['src/blocks/**/src/**/*'], ['clean:img', 'images', browser.reload]);
});