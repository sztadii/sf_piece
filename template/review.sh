#!/bin/bash

print() {
    cssWeight="$(ls -lh ./dist/assets/css/app.css | awk '{print $5}')"
    jsWeight="$(ls -lh ./dist/assets/js/app.js | awk '{print $5}')"
    imgWeight="$(du -sh ./dist/assets/img | awk '{print $1}')"
    htmlWeight="$(du -sh ./dist/pageMain.html | awk '{print $1}')"

    echo CSS size: "${cssWeight}"
    echo JS size: "${jsWeight}"
    echo IMG size: "${imgWeight}"
    echo HTML size: "${htmlWeight}"
}

print