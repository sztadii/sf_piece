'use strict';

function initPartnersSlick() {
    var slickBox = $('.partners__content.-slick');

    if (slickBox.length > 0) {
        slickBox.imagesLoaded().done(function () {
            slickBox.slick({
                draggable: false,
                infinite: true,
                dots: false,
                speed: 700,
                slidesToShow: 4,
                slidesToScroll: 4,
                arrows: true,
                prevArrow: '<div class="slick__arrow -left slick-prev"><i class="slick__icon fa fa-angle-left "></i></div>',
                nextArrow: '<div class="slick__arrow -right slick-next"><i class="slick__icon fa fa-angle-right"></i></div>',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                        }
                    }, {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }, {
                        breakpoint: 320,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        });
    }
}

$(document).ready(function () {
    initPartnersSlick();
});