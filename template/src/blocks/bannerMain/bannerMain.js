'use strict';

function initBannerMainSlick() {
    var slickBox = $('.bannerMain .bannerMain__slick.-slick');

    if (slickBox.length > 0) {
        slickBox.imagesLoaded().done(function () {

            bannerHeightAdjust();

            slickBox.slick({
                infinite: true,
                dots: false,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                fade: true,
                adaptiveHeight: true,
                autoplay: true,
                autoplaySpeed: 4000
            });
        });
    }
}

function goToContent() {
    var trigger = $('.bannerMain__trigger');

    trigger.on('click', function (e) {
        var length = $('.bannerMain').height() + 4;

        e.preventDefault();

        $('html,body').animate({
            scrollTop: length
        }, 1000);
    });
}


function getWindowHeight() {
    return window.innerHeight ? window.innerHeight : $(window).height()
}

function bannerHeightAdjust() {
    var header = $('header').outerHeight();
    var window = getWindowHeight();
    var calculatedHeight = window - header;

    $('.bannerMain__item').height(calculatedHeight);
}

$(document).ready(function () {
    initBannerMainSlick();
    goToContent();
});

$(window).resize(function () {
    bannerHeightAdjust();
});