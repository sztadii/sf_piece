function initLoader() {
    var loader = $(".loader");
    setTimeout(function () {
        loader.addClass('hide');

        setTimeout(function () {
            loader.remove();
        }, 2000);
    }, 500);
}

$(document).ready(function () {
    initLoader();
});