function initProducktSlick() {
    var slickBox = $('.product .product__gallery');

    if (slickBox.length > 0) {
        slickBox.imagesLoaded().done(function () {
            slickBox.slick({
                infinite: true,
                dots: false,
                arrows: true,
                prevArrow: '<div class="slick__arrow -left slick-prev"><i class="slick__icon fa fa-angle-left "></i></div>',
                nextArrow: '<div class="slick__arrow -right slick-next"><i class="slick__icon fa fa-angle-right"></i></div>',
                slidesToShow: 1,
                slidesToScroll: 1
            });
        });
        slickBox.on('init', function (event, slick, currentSlide, nextSlide) {
            slickBox.append('<div class="product__pagingInfo"></div>');
            var i = (currentSlide ? currentSlide : 0) + 1;

            $('.product__pagingInfo').text(i + '/' + slick.slideCount);
        });
        slickBox.on('afterChange', function (event, slick, currentSlide, nextSlide) {
            var i = (currentSlide ? currentSlide : 0) + 1;

            $('.product__pagingInfo').text(i + '/' + slick.slideCount);
        });
    }
}

$(document).ready(function () {
    initProducktSlick();
});