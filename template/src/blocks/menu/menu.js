'use strict';

var menuBreakpoint = 1024;
var body;
var menuButton;
var menuBackground;
var menuListMain;

function hideMenu() {
    menuBackground.fadeOut();
    menuListMain.fadeOut(200, function () {
        menuButton.removeClass('-active');
    });
}

function showMenu() {
    menuBackground.fadeIn();
    menuListMain.fadeIn(200, function () {
        menuButton.addClass('-active');
    });
}

function resetMenu() {
    if (body.width() > menuBreakpoint) {
        menuButton.removeClass('-active');
        menuListMain.removeAttr('style');
        menuBackground.fadeOut();
    }
}

function toggleMenu() {
    if (body.width() <= menuBreakpoint) {
        if (menuButton.hasClass('-active')) {
            hideMenu();
        } else {
            showMenu();
        }
    }
}

function initSuperfish() {
    if (body.width() > menuBreakpoint) {
        menuListMain.supersubs(({
            minWidth: 12,
            maxWidth: false,
            extraWidth: 1
        })).superfish({
            delay: 2000,
            cssArrows: false
        });
    }
}

function checkSuperfish() {
    if (body.width() > menuBreakpoint && !menuListMain.hasClass('sf-js-enabled')) {
        initSuperfish();
        menuListMain.show().attr('style', '').css('display', 'block');
        $('.menu .menu__list .act').removeClass('act');
    } else if (body.width() < menuBreakpoint) {
        $('.menu__list').attr('style', '');
        hideMenu();
        menuListMain.superfish('destroy');
    }
}

$(document).ready(function () {
    body = $("body");
    menuButton = $('.menu__button');
    menuBackground = $('.menu .menu__background');
    menuListMain = $('.menu .menu__list.-main');

    initSuperfish();

    menuButton.on('click', function () {
        toggleMenu();
    });

    menuBackground.on('click', function () {
        toggleMenu();
    });

    $('.menu .menu__list').on('click', function (event) {
        event.stopPropagation();

        if ($(this).hasClass('-active')) {
            $(this).removeClass('-active').children().slideUp(400);
        } else {
            $(this).parent().siblings().find('.menu__list.-active:not(.-main)').removeClass('-active').children().slideUp();
            $(this).addClass('-active').children().slideDown(400);
        }
    });
});

$(window).resize(function () {
    checkSuperfish();
    resetMenu();
});