'use strict';

(function ($) {
    $.fn.resizeToParent = function (opts) {
        var defaults = {
            parent: 'div',
            delay: 1000
        };

        var opts = $.extend(defaults, opts);

        function positionImage(obj) {

            obj.css({'width': '', 'height': '', 'margin-left': '', 'margin-top': ''});

            var parentWidth = obj.parents(opts.parent).width();
            var parentHeight = obj.parents(opts.parent).height();

            var imageWidth = obj.width();
            var imageHeight = obj.height();

            var diff = imageWidth / parentWidth;

            if ((imageHeight / diff) < parentHeight) {
                obj.css({'width': 'auto', 'height': parentHeight});

                imageWidth = imageWidth / (imageHeight / parentHeight);
                imageHeight = parentHeight;
            }
            else {
                obj.css({'height': 'auto', 'width': parentWidth});

                imageWidth = parentWidth;
                imageHeight = imageHeight / diff;
            }

            var leftOffset = (imageWidth - parentWidth) / -2;
            var topOffset = (imageHeight - parentHeight) / -2;
            obj.css({'margin-left': leftOffset, 'margin-top': topOffset});
        }


        var tid;
        var elems = this;
        $(window).on('resize', function () {
            clearTimeout(tid);
            tid = setTimeout(function () {
                elems.each(function () {
                    positionImage($(this));
                });
            }, opts.delay);
        });
        return this.each(function () {
            var obj = $(this);

            obj.attr('src', obj.attr('src'));

            obj.load(function () {
                positionImage(obj);
            });

            if (this.complete) {
                positionImage(obj);
            }
        });
    }

})(jQuery);
