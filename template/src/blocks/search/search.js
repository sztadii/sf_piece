function initSearcher() {
    var searcher = {
        init: function () {
            this.renderSearch();
            this.generatePanel();
        },
        renderSearch: function () {
            this.$el = $('.search');
            this.$button = this.$el.find('.search__button');
            this.$initer = this.$el.find('.search__panelIniter');
            this.$input = this.$el.find('.search__textbox');
        },
        generatePanel: function () {
            this.$initer.on('click', function () {
                $(this).css('display', 'none');
                searcher.$button.fadeIn().css('display', 'block');
                searcher.$input.fadeIn().css({'display': 'block', 'right': '50px', 'position': 'absolute'})
            })
        }
    };

    searcher.init();
}

$(document).ready(function () {
    initSearcher();
});