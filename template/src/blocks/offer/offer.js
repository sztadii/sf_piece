'use strict';

function initOfferSlick() {
    var slickBox = $('.container__content.-slick');

    if (slickBox.length > 0) {
        slickBox.imagesLoaded().done(function () {
            slickBox.slick({
                draggable: false,
                infinite: true,
                dots: true,
                customPaging: function () {
                    return '<div class="slick__dots"></div>';
                },
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                arrows: true,
                prevArrow: '<div class="slick__arrow -left slick-prev"><i class="slick__icon fa fa-angle-left "></i></div>',
                nextArrow: '<div class="slick__arrow -right slick-next"><i class="slick__icon fa fa-angle-right"></i></div>',
                slide: '.offer__item',
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                }, {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }, {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                ]
            });
        });
    }
}
$(document).ready(function () {
    initOfferSlick();
});