'use strict';

function initBannerBottomSlick() {
    var slickBox = $('.bannerBottom__content.-slick');

    if (slickBox.length > 0) {
        slickBox.imagesLoaded().done(function () {
            slickBox.slick({
                draggable: true,
                infinite: true,
                dots: true,
                customPaging: function () {
                    return '<div class="slick__dots"></div>';
                },
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                prevArrow: '<div class="slick__arrow -left slick-prev"><i class="slick__icon fa fa-angle-left "></i></div>',
                nextArrow: '<div class="slick__arrow -right slick-next"><i class="slick__icon fa fa-angle-right"></i></div>',
                centerMode: false,
                autoplay: false
            });
        });
    }
}


$(document).ready(function () {
    initBannerBottomSlick();
});
